# Kickoff

## Task
* A RESTful API for searching GitHub users by:
    - username
    - location
    - programming language

* Users retrieved via the Search API should be written to database and the documents should be 
updated as new/missing information arrives.

## Initial thoughts
1. Need to read GitHub documentation, have never worked with their API.
2. Draft API spec, maybe in Swagger (bonus points)
3. Refashion the spec into tests
4. Get cracking!

## Progress

- [x] Hello, world! in Express
- [x] Wrote a request logger middleware
- [x] Wrote a error logger middleware
- [x] Added GitHub connector
- [x] GitHub connector unit tested
- [x] App and database hosted in Heroku
- [x] Basic Mongoose setup
- [ ] Authentication middleware 

## Thinking out loud

### Moving parts
![Architecture](./assets/Kick-off-architecture.png)

### Flow of Control
![Flow of Control](./assets/flow-of-control.png)