/**
    user.js
    Kickoff

    Created on 21 October 2018 by Animesh Mishra <hello@animesh.ltd>
    © 2018 Animesh Mishra. All Rights Reserved.
**/

const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
    login: String,
    id: Number,
    node_id: String,
    avatar_url: String,
    gravatar_id: String,
    url: String,
    html_url: String,
    followers_url: String,
    following_url: String,
    gists_url: String,
    starred_url: String,
    subscriptions_url: String,
    organizations_url: String,
    repos_url: String,
    events_url: String,
    received_events_url: String,
    type: String,
    site_admin: String,
    score: Number
})

const User = mongoose.model("User", userSchema)

module.exports = User