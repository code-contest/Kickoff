/**
    index.js
    Kickoff

    Created on 21 October 2018 by Animesh Mishra <hello@animesh.ltd>
    © 2018 Animesh Mishra. All Rights Reserved.
**/

const fs            = require("fs")
const https         = require("https")
const querystring   = require("querystring")

// Make the GitHub access token available to the app if it hasn't
// already been set by the hosting environment.
// Used when running the app locally.
if (!process.env.github_token) {
    process.env.github_token = fs.readFileSync("token").toString()
}

/**
 *  Call the GitHub Search Users API with the given parameters.
 *  See: https://developer.github.com/v3/search/#search-users
 * 
 *  @param {string} username    GitHub username
 *  @param {string} location    GitHub profile location
 *  @param {string} language    Repository language
 */
function Search(username, location, language) { 
    let query = buildQuery(username, location, language)

    let options = {
        hostname: "api.github.com",
        path: `/search/users?q=${query}`,
        method: "GET",
        headers: {
            "Authorization": `token ${process.env.github_token}`,
            "User-Agent": "Kickoff"
        }
    }

    return new Promise((resolve, reject) => {
        let result = ""
        let request = https.request(options, (response) => {
            response.on("data", chunk => result += chunk)
            
            response.on("end", () => {
                if (response.statusCode == 200) {
                    resolve(JSON.parse(result))
                }
                
                reject(result)
            })
        })
    
        request.on("error", error => console.log(`GitHub API request failed: ${error.message}`))
        request.end()
    })
}

function buildQuery(username, location, language) {
    let params = {} 
    
    if (username) {
        params.username = username
        params.in = "login"
    }

    if (location) {
        params.location = location
    }

    if (language) {
        params.language = language
    }

    params.type = "user"

    let query = querystring.stringify(params, "+", ":")

    // If a username is specified, querystring.stringify produces a string in the 
    // shape "username:<username>+in:login+...". The GitHub API expects the string 
    // to be of the form "<username>+in:login+...". The "username:" portion at the 
    // beginning of the string is extraneous.
    return username ? query.slice(9) : query
}

module.exports.Search = Search