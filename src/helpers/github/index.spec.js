/**
    index.spec.js
    Kickoff

    Created on 21 October 2018 by Animesh Mishra <hello@animesh.ltd>
    © 2018 Animesh Mishra. All Rights Reserved.
**/

const assert = require("assert")
const GitHub = require("./index")
 
describe("GitHub Connector", function() {
    this.timeout(10000)

    describe("# Search()", function() {

        it("should return a non-empty array when no parameter is provided.", async function() {
            let result = await GitHub.Search()
            assert.notEqual(result.total_count, 0)
        })

        it("should return my GitHub profile when my username is provided.", async function() {
            let result = await GitHub.Search("SirAnimesh")
            assert.notEqual(result.total_count, 0)
            assert.equal(result.items[0].id, 12829119)
        })

        it("should return my GitHub profile when my location is provided.", async function() {
            // GitHub API supports UTF-8, पुर्तगालम्‌ is Sanskrit for Portugal.
            // I'm assuming I'm the only one in Portugal using Sanskrit for location.
            // This test can be improved.
            let result = await GitHub.Search(null, "पुर्तगालम्‌")
            assert.notEqual(result.total_count, 0)
            assert.equal(result.items[0].id, 12829119)
        })

        it("should return a non-empty array when Scilab language is provided.", async function() {
            let result = await GitHub.Search(null, null, "Scilab")
            assert.notEqual(result.total_count, 0)
        })

        it("should return a non-empty array when all the parameters are provided.", async function() {
            let result = await GitHub.Search("tom", "barcelona", "javascript")
            assert.notEqual(result.total_count, 0)
        })

    })
})