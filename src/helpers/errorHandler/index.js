/**
    index.js
    Kickoff

    Created on 20 October 2018 by Animesh Mishra <hello@animesh.ltd>
    © 2018 Animesh Mishra. All Rights Reserved.
**/

const fs    = require("fs")
const path  = require("path")

function errorHandler(error, request, response, next) {
    let logMessage = {
        timestamp: new Date().toUTCString(),
        request: {
            method: request.method,
            url: request.originalUrl,
            headers: request.headers,
            query: request.params
        },
        error: error.message
    }
    let output = "···················\n" + JSON.stringify(logMessage, null, 2) + "\n"

    let logFilePath = path.join(__dirname, "../../..", ".logs", "error.log")
    fs.appendFile(logFilePath, output, (error) => {
        if (error) { throw error }
    })

    response.status(500).end("Internal Server Error")
}

module.exports = errorHandler