/**
    index.js
    Kickoff

    Created on 20 October 2018 by Animesh Mishra <hello@animesh.ltd>
    © 2018 Animesh Mishra. All Rights Reserved.
**/

const fs    = require("fs")
const path  = require("path")

function logger(request, response, next) {
    let logMessage = {
        timestamp: new Date().toUTCString(),
        source: request.ip,
        request: {
            method: request.method,
            url: request.originalUrl,
            headers: request.headers,
            query: request.query
        }
    }
    let output = "···················\n" + JSON.stringify(logMessage, null, 2) + "\n"

    let logFilePath = path.join(__dirname, "../../..", ".logs", "request.log")
    fs.appendFile(logFilePath, output, (error) => {
        if (error) { next(error) }
        next()
    })
}

module.exports = logger