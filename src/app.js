/**
    app.js
    Kickoff

    Created on 20 October 2018 by Animesh Mishra <hello@animesh.ltd>
    © 2018 Animesh Mishra. All Rights Reserved.
**/

const express      = require("express")
const errorHandler = require("./helpers/errorHandler")
const fs           = require("fs")
const logger       = require("./helpers/logger")
const mongoose     = require("mongoose")
const search       = require("./routes/search")

const app = express()

app.use(logger)
app.get("/", (request, response, next) => response.end("Hello, world!"))
app.get("/search", search.GetUsers)
app.use(errorHandler)

if (!process.env.db_conn_url) {
    process.env.db_conn_url = fs.readFileSync("db").toString()
}
mongoose.connect(process.env.db_conn_url)
const db = mongoose.connection

db.on("error", console.error.bind(console, "MongoDB connection error:"))

db.once("open", () => {
    console.log("Database connection established.")
    
    let port = process.env.PORT || 3000
    app.listen(port, () => console.log(`Server running at localhost:${port}...`))
})
