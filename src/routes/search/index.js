/**
    index.js
    Kickoff

    Created on 21 October 2018 by Animesh Mishra <hello@animesh.ltd>
    © 2018 Animesh Mishra. All Rights Reserved.
**/

const GitHub = require("../../helpers/github")
const User   = require("../../schema/user")

function GetUsers(request, response, next) {
    let $ = request.query
    GitHub.Search($.username, $.location, $.language)
    
    .then( result => {
        if (result.total_count) {
            writeToDb(result.items)
            // Send result.items in response instead of waiting for
            // database operation to complete
            response.status(200).json(result.items)
        }
        else {
            response.status(200).json([])
        }
    })
    
    .catch( result => {
        next(new Error(result.toString()))
    })
}

function writeToDb(items) {
    for (let item of items) {
        let query = { id: item.id }
        let options = {
            new: true,      // return the modified document rather than the original
            upsert: true    // create the object if it doesn't exist
        }

        User.findOneAndUpdate(query, item, options, (error, doc) => {
            if (error) { 
                console.log(`Database error: ${error}`)
                next(error) 
            }
        })
    }
}

module.exports.GetUsers = GetUsers